FROM node:14
EXPOSE 3000 3443
WORKDIR /app

COPY yarn.lock package.json ./

RUN yarn install

COPY . .

RUN chmod +x ./wait-cert.sh

CMD ./wait-cert.sh
