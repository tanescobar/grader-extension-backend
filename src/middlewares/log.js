const LogRepository = require('../repositories/log');

exports.prepareContext = (req, res, next) => {
    req.logContext = {
        startTime: new Date(),
    };
    next();
};

exports.logSuccess = async (req, res, next) => {
    const {startTime, description} = req.logContext;
    const latency = new Date().getTime() - startTime.getTime();
    await LogRepository.write(req.path, res.statusCode, req.body.id, latency, description);
    next();
};

exports.logError = async (err, req, res, next) => {
    const statusCode = err.status || 500;

    const {startTime} = req.logContext;
    const latency = new Date().getTime() - startTime.getTime();
    await LogRepository.write(
        req.path,
        statusCode,
        req.body.id,
        latency,
        `error message='${err.message || 'unexpected error'}' with reqbody=${JSON.stringify(req.body)}`
    );

    res.status(statusCode).json({
        statusCode,
        message: err.message || 'unexpected error',
    });
    next(err);
}