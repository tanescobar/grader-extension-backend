const QuizRepository = require('../repositories/quiz');

class QuizNotFound extends Error {
    constructor() {
        super("Quiz not found");
        this.status = 404;
    }
}

exports.prepareContext = async (req, res, next) => {
    try {
        const currentQuiz = await QuizRepository.getCurrentQuiz();
        if (currentQuiz === undefined) {
            throw new QuizNotFound();
        }
        req.quizContext = {
            startTime: currentQuiz.time.getNanoTime(),
            description: currentQuiz.description
        }
        next();
    } catch (error) {
        next(error);
    }
}