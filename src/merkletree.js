const sha256 = require('crypto-js/sha256');
const hex = require('crypto-js/enc-hex');

class InvalidMerkleTreeElement extends Error {
    constructor() {
        super('Invalid merkle tree element');
        this.status = 500;
    }
}

module.exports = class MerkleTree extends Array {

    static hash = sha256;

    /**
     *
     * If Odd, the last are duplicated and pair with itself, i.e.
     *
     *              ROOT
     *                |
     *      ---------------------
     *      |                   |
     *   ---------         -----------
     *   |       |         |         |
     *   0       1         2         2
     *
     *  The process loop until there is no more than 1 in result array.
     */

    async merkleDigest() {
        if (this.length === 0) {
            return {
                digest: undefined,
                item: undefined,
                left: null,
                right: null,
            };
        }

        if (this.some(item => typeof item !== 'string' && !(item instanceof MerkleTree))) {
            throw new InvalidMerkleTreeElement();
        }

        let currentList = await Promise.all(this.map(async (item) => {
            if (item instanceof MerkleTree) {
                return await item.merkleDigest();
            } else {
                return {
                    digest: hex.stringify(MerkleTree.hash(item)),
                    item,
                    left: null,
                    right: null,
                }
            }
        }));

        while (currentList.length > 1) {
            const newList = [];

            if (currentList.length & 1) { //if odd
                currentList.push(currentList[currentList.length - 1]); //push duplicate
            }

            for (let i = 0; i < currentList.length; i += 2) {
                const left = currentList[i];
                const right = currentList[i + 1];
                const item = left.digest.concat(right.digest);
                newList.push({
                    digest: hex.stringify(MerkleTree.hash(item)),
                    item,
                    left,
                    right,
                })
            }

            currentList = newList
        }

        return currentList[0];
    }
}
