const Express = require('express');
const Router = Express.Router();

const MerkleTree = require('../merkletree');
const Stellar = require('../stellar.js');

const QuizRepository = require('../repositories/quiz');
const SubmissionRepository = require('../repositories/submission');

const QuizMiddleware = require('../middlewares/quiz');

class InvalidOpenQuizRequestBody extends Error {
    constructor() {
        super('Invalid open quiz request body');
        this.status = 400;
    }
}

class QuizIsOpening extends Error {
    constructor() {
        super('Quiz is opening');
        this.status = 400;
    }
}

class DuplicateQuizDescription extends Error {
    constructor() {
        super('Duplicate quiz description');
        this.status = 409;
    }

}

const open = async (req, res, next) => {
    try {
        const {description} = req.body;
        if (description === undefined) {
            throw new InvalidOpenQuizRequestBody();
        }

        const currentQuiz = await QuizRepository.getCurrentQuiz();
        if (currentQuiz !== undefined) {
            throw new QuizIsOpening();
        }

        const quiz = await QuizRepository.getTimeInterval(description);
        if (quiz !== undefined) {
            throw new DuplicateQuizDescription();
        }

        const descriptionDigest = (await MerkleTree.of(description).merkleDigest()).digest;
        const transactionID = await Stellar.saveMemo(descriptionDigest);

        await QuizRepository.open(description, transactionID);

        req.logContext.description = `description='${description}' txnid=${transactionID}`;

        res.status(200).send({
            status: 200,
            message: `Open quiz '${description}' successfully`,
            transactionID,
        });
        next();
    } catch (error) {
        next(error);
    }
}

const close = async (req, res, next) => {
    try {
        const {startTime, description} = req.quizContext;

        const submissions = await SubmissionRepository.findAllSubmission(startTime, 'now()');

        for (const group of submissions.groupRows) {
            const {rows, tags} = group;
            const {id} = tags;
            const merkleTree = MerkleTree.of(...rows.map(submission => {
                return MerkleTree.of(
                    submission.time.getNanoTime(),
                    submission.id,
                    submission.publicKey,
                    submission.signedCode,
                );
            }));
            const submissionsDigest = (await merkleTree.merkleDigest()).digest;
            const transactionID = await Stellar.saveMemo(submissionsDigest);
            await SubmissionRepository.pack(id, transactionID);
        }

        const descriptionDigest = (await MerkleTree.of(description).merkleDigest()).digest;
        const transactionID = await Stellar.saveMemo(descriptionDigest);

        await QuizRepository.close(description, transactionID);

        req.logContext.description = `description='${description}' txnid=${transactionID}`;

        res.status(200).send({
            status: 200,
            message: `Close quiz '${req.quizContext.description}' successfully`,
            transactionID,
        });
        next();
    } catch (error) {
        next(error);
    }
}

Router.post('/open', open);
Router.post('/close', QuizMiddleware.prepareContext, close);

module.exports = Router;