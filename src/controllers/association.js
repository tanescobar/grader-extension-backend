const Express = require('express');
const Router = Express.Router();

const QuizMiddleware = require('../middlewares/quiz');
const MerkleTree = require('../merkletree.js');
const Stellar = require('../stellar.js');

const AssociationRepository = require('../repositories/association');

class InvalidCreateAssociationRequestBody extends Error {
    constructor() {
        super("Invalid create association request body");
        this.status = 400;
    }
}

class DuplicateAssociation extends Error {
    constructor() {
        super("Duplicate association");
        this.status = 409;
    }
}

const create = async (req, res, next) => {
    try {
        const {id, publicKey} = req.body;
        if (id === undefined || publicKey === undefined) {
            throw new InvalidCreateAssociationRequestBody();
        }
        if (await AssociationRepository.exists(id, publicKey)) {
            throw new DuplicateAssociation();
        }
        const associationDigest = (await MerkleTree.of(id, publicKey).merkleDigest()).digest;
        const transactionID = await Stellar.saveMemo(associationDigest);

        await AssociationRepository.save(id, publicKey, transactionID);

        req.logContext.description = `publickey=${publicKey}, txnid=${transactionID}`;

        res.status(200).send({
            status: 200,
            message: 'create association successfully',
            transactionID,
        });
        next();
    } catch (error) {
        next(error);
    }
}

Router.post('/create', QuizMiddleware.prepareContext, create);

module.exports = Router;
