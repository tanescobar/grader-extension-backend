const Influx = require('influx');
const InfluxSchema = require('./schema.js');

module.exports = new Influx.InfluxDB({
    host: process.env.INFLUX_HOST || 'localhost',
    database: process.env.INFLUX_DATABASE || 'grader_extension',
    username: process.env.INFLUX_ADMIN_USERNAME || '',
    password: process.env.INFLUX_ADMIN_PASSWORD || '',
    schema: Object.values(InfluxSchema)
});
