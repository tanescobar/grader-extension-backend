const InfluxDB = require('./datasources/influxdb');
const InfluxSchema = require('./datasources/schema');

exports.write = async (path, status, id, latency, description) => {
    await InfluxDB.writePoints([{
        measurement: InfluxSchema.Log.measurement,
        fields: {
            latency, description
        },
        tags: {
            path, status, id
        }
    }]);
};

exports.read = async () => {
    return await InfluxDB.query(`
        select * from ${InfluxSchema.Log.measurement}
        order by time desc limit 30
    `);
}
